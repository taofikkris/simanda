<?php 
class Model_app extends CI_model{

    var $tabel_pos='pos';
    var $tabel_subpos='subpos';
    var $tabel_subkegiatan='subkegiatan';

    public function __construct(){
    parent::__construct();
    }

   public function ambil_pos() {
    $sql_pos=$this->db->get($this->tabel_pos);  
    if($sql_pos->num_rows()>0){
        foreach ($sql_pos->result_array() as $row)
            {
                $result['-']= '- Pilih Pos -';
                $result[$row['id_pos']]= ucwords(strtoupper($row['kode_pos'].' - '.$row['nama_pos']));
            }
            return $result;
        }
    }
    
    public function ambil_subpos($kode_pos){
    $this->db->where('id_pos',$kode_pos);
    $this->db->order_by('kode_subpos','asc');
    $sql_subpos=$this->db->get($this->tabel_subpos);
    if($sql_subpos->num_rows()>0){

        foreach ($sql_subpos->result_array() as $row)
        {
            $result[$row['id_subpos']]= ucwords(strtoupper($row['kode_subpos'].' - '.$row['nama_subpos']));
        }
        } else {
           $result['-']= '- Belum Ada Subpos Kegiatan -';
        }
        return $result;
    }
    
    public function ambil_subkegiatan($kode_subpos){
    $this->db->where('id_subpos',$kode_subpos);
    $this->db->order_by('poske','asc');
    $sql_subkegiatan=$this->db->get($this->tabel_subkegiatan);
    if($sql_subkegiatan->num_rows()>0){

        foreach ($sql_subkegiatan->result_array() as $row)
        {
            $result[$row['id_kegiatan']]= ucwords(strtolower($row['poske'].' - '.$row['nama_kegiatan']));
        }
        } else {
           $result['-']= '- Belum Ada Sub Kegiatan -';
        }
        return $result;
    }

    public function view($table){
        return $this->db->get($table);
    }

    public function insert($table,$data){
        return $this->db->insert($table, $data);
    }

    public function edit($table, $data){
        return $this->db->get_where($table, $data);
    }
 
    public function update($table, $data, $where){
        return $this->db->update($table, $data, $where); 
    }

    public function delete($table, $where){
        return $this->db->delete($table, $where);
    }

    public function view_where($table,$data){
        $this->db->where($data);
        return $this->db->get($table);
    }

    public function countpos($table,$where,$iki){
        $this->db->select('count(*) as cpos');
        $this->db->from($table);
        $this->db->where($where,$iki);
        $res= $this->db->get();
        return $res->row_array();
    }

    public function countsubpos($table,$where,$iki){
        $this->db->select('count(*) as csubpos');
        $this->db->from('subpos');
        $this->db->where($where,$iki);
        $res= $this->db->get();
        return $res->row_array();
    }

    public function countsubposke($table,$where,$iki){
        $this->db->select('count(*) as csubposke');
        $this->db->from('subkegiatan');
        $this->db->where($where,$iki);
        $res= $this->db->get();
        return $res->row_array();
    }

    public function countrealisasi($table,$where,$iki){
        $this->db->select('count(*) as creal');
        $this->db->from('realisasi');
        $this->db->where($where,$iki);
        $res= $this->db->get();
        return $res->row_array();
    }

    public function view_where_like($table,$data,$like){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($data);
        $this->db->like($like);
        return $this->db->get()->result_array();
    }

    public function view_where_likek($table,$data,$iki){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($iki,$data);
        return $this->db->get()->result_array();
    }

    public function view_ordering_limit($table,$order,$ordering,$baris,$dari){
        $this->db->select('*');
        $this->db->order_by($order,$ordering);
        $this->db->limit($dari, $baris);
        return $this->db->get($table);
    }
    
    public function view_ordering($table,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_ordering_tahun($table,$order,$ordering,$data,$iki){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($data,$iki);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_where_ordering($table,$data,$order,$ordering){
        $this->db->where($data);
        $this->db->order_by($order,$ordering);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function view_join_one($table1,$table2,$field,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function get_by($id)
    {
        $this->db->order_by('p.id_subpos','ASC');
        $this->db->where('p.id_pos',$id);
        $this->db->join('t_pos as b','p.id_pos=b.id_pos');
        return $this->db->get('t_subpos as p');
    }

    public function view_join_one_tahun($table1,$table2,$field,$order,$ordering,$where,$iki){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($table1.'.'.$where,$iki);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_where($table1,$table2,$field,$where,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_where_akses($table1,$table2,$field,$where,$iki,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($iki,$where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_where_tahun($table1,$table2,$field,$where,$order,$ordering,$iki){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($iki,$where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_two($table1,$table2,$table3,$field1,$field2,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    // public function view_join_two_distinct(){
    //     $res = $this->db->query("SELECT DISTINCT pos.kode_pos FROM `pos` JOIN `subpos` ON `pos`.`id_pos`=`subpos`.`id_pos` JOIN `subkegiatan` ON `subpos`.`id_subpos`=`subkegiatan`.`id_subpos` ORDER BY `kode_pos` ASC");
    //     return $res;
    // }

    public function view_join_two_where($table1,$table2,$table3,$field4,$field2,$where,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->where($where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_tiga($table1,$table2,$table3,$table4,$field1,$field2,$field3,$order,$ordering,$where){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->join($table4, $table3.'.'.$field3.'='.$table4.'.'.$field3);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_tiga_tahun($table1,$table2,$table3,$table4,$field1,$field2,$field3,$order,$ordering,$where,$iki){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->join($table4, $table3.'.'.$field3.'='.$table4.'.'.$field3);
        $this->db->where($table2.'.'.$where,$iki);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

     public function view_join_tiga_sum($table1,$table2,$table3,$table4,$field1,$field2,$field3,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->join($table4, $table3.'.'.$field3.'='.$table4.'.'.$field3);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_tiga_where($table1,$table2,$table3,$table4,$field1,$field2,$field3,$where,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->join($table4, $table3.'.'.$field3.'='.$table4.'.'.$field3);
        $this->db->where($where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_empat($table1,$table2,$table3,$table4,$table5,$field1,$field2,$field3,$field4,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field1);
        $this->db->join($table3, $table2.'.'.$field2.'='.$table3.'.'.$field2);
        $this->db->join($table4, $table3.'.'.$field3.'='.$table4.'.'.$field3);
        $this->db->join($table5, $table4.'.'.$field4.'='.$table5.'.'.$field4);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    function umenu_akses($link,$id){
        return $this->db->query("SELECT * FROM modul,users_modul WHERE modul.id_modul=users_modul.id_modul AND users_modul.id_session='$id' AND modul.link='$link'")->num_rows();
    }

    public function cek_login($username,$password,$table){
        return $this->db->query("SELECT * FROM $table where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."' AND blokir='N'");
    }

    function tampil_data($vtanggal){
        $vbulan = date("m",strtotime($vtanggal)); 
        $this->db->select('*');
        $this->db->from('realisasi');
        $this->db->where('month(tgl)',$vbulan);
        $this->db->where('year(tgl)',$vtanggal);
        $query = $this->db->get();
        return $query;
    }

    function tampil_data1($vtanggal){
        $vbulan = date("m",strtotime($vtanggal)); 
        $this->db->select('*');
        $this->db->from('realisasi');
        $this->db->where('month(tgl)',$vbulan);
        $this->db->where('year(tgl)',$vtanggal);
        $query = $this->db->get();
        return $query;
    }
}