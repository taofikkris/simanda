<!-- User Info -->
<div class="user-info">
    <div class='image'>
        <?php $usr = $this->Model_app->view_where('users', array('username'=> $this->session->username))->row_array();
            if (trim($usr['foto'])==''){ $foto = 'blank.png'; }else{ $foto = $usr['foto']; } ?>
        <img src='<?php echo base_url(); ?>/assets/foto_user/<?php echo $foto; ?>' width='48' height='48' alt='User' >
    </div>
    <div class='info-container'>
        <div class='name' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><?php echo $usr['nama_lengkap'] ?></div>
        <div class='email'><?php echo $usr['email'] ?></div>
    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <?php $tahun = $this->Model_app->view_where('tahun', array('id_tahun'=> $this->session->tahun))->row_array();?>
        <li class="header">TAHUN ANGGARAN <?php echo $tahun['nama_tahun'] ?></li>
        <li class="active">
            <a href="<?php echo base_url() ?>administrator/home">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        <li>
            <?php 
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek') {
             echo "<a href='javascript:void(0);'' class='menu-toggle'>
                <i class='material-icons'>attach_money</i>
                <span>Rencana Anggaran</span>
            </a>"; 
            }  
            ?>
            
            <ul class="ml-menu">
                <?php
                    $cek=$this->Model_app->umenu_akses("manajemenpos",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajemenpos'><i class='material-icons'>chevron_right</i> Master Pos</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("manajemensubpos",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajemensubpos'><i class='material-icons'>chevron_right</i> Master Sub Pos</a></li>";
                    }

                     $cek=$this->Model_app->umenu_akses("manajemensubbidangpos",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajemensubposke'><i class='material-icons'>chevron_right</i> Master Sub Bidang Pos</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("manajementahun",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajementahun'><i class='material-icons'>chevron_right</i> Master Tahun Anggaran</a></li>";
                    }
                ?>
            </ul>
        </li>
        
        <li>
            <?php 
            if ($this->session->level=='user') {
             echo "<a href='".base_url().$this->uri->segment(1)."/realisasi'><i class='material-icons'>payment</i>
                <span>Realisasi Anggaran</span>
                </a>";   
            }
            ?>
        </li>

        <li>
            <?php 
            if ($this->session->level=='admin' OR $this->session->level=='wadek' OR $this->session->level=='dekan' OR $this->session->level=='user2' OR $this->session->level=='user') {
             echo "<a href='javascript:void(0);' class='menu-toggle'>
                <i class='material-icons'>account_balance_wallet</i>
                <span>Saldo</span>
            </a>";   
            }
            ?>
            
            <ul class="ml-menu">
                <?php
                    $cek=$this->Model_app->umenu_akses("saldopos",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/saldopos'><i class='material-icons'>chevron_right</i> Saldo Per Pos</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("saldosubpos",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/saldosubpos'><i class='material-icons'>chevron_right</i> Saldo Per Sub Pos</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("saldoposkegiatan",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/saldoposkegiatan'><i class='material-icons'>chevron_right</i> Saldo Per Pos Kegiatan</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("saldoall",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/saldoall'><i class='material-icons'>chevron_right</i> Saldo Keseluruhan</a></li>";
                    }
                ?>
            </ul>
        </li>
        <li>
            <?php 
            if ($this->session->level=='admin' OR $this->session->level=='wadek' OR $this->session->level=='user') {
             echo " <a href='javascript:void(0);' class='menu-toggle'>
                <i class='material-icons'>print</i>
                <span>Laporan</span>
            </a>";   
            }
            ?>
            <ul class="ml-menu">
                <?php
                    $cek=$this->Model_app->umenu_akses("manajemenmodul",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/laporan'><i class='material-icons'>chevron_right</i> Laporan Keuangan</a></li>";
                    }
                ?>
            </ul>
            <ul class="ml-menu">
                <?php
                    $cek=$this->Model_app->umenu_akses("manajemenmodul",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/laporanall'><i class='material-icons'>chevron_right</i> Laporan Keseluruhan</a></li>";
                    }
                ?>
            </ul>
        </li>
        <li>
            <?php 
            if ($this->session->level=='admin' OR $this->session->level=='wadek') {
             echo "<a href='javascript:void(0);' class='menu-toggle'>
                <i class='material-icons'>person</i>
                <span>Modul User</span>
            </a>";   
            }
            ?>
            
            <ul class="ml-menu">
                <?php
                    $cek=$this->Model_app->umenu_akses("manajemenuser",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajemenuser'><i class='material-icons'>chevron_right</i> Manajemen User</a></li>";
                    }

                    $cek=$this->Model_app->umenu_akses("manajemenmodul",$this->session->id_session);
                    if($cek==1 OR $this->session->level=='admin'){
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/manajemenmodul'><i class='material-icons'>chevron_right</i> Manajemen Modul</a></li>";
                    }
                ?>
            </ul>
        </li>
        
        
    </ul>
</div>

<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        <strong>&copy; <?php echo date('Y'); ?> All rights reserved.</strong> 
    </div>
    <div class="version">
        Developer By <b><a target='_BLANK' href="http://kelas-coding.blogspot.com/"> Taofik Krisdiyanto-18330059</a></b>.
    </div>
</div><strong>Copyright &copy; <?php echo date('Y'); ?> <a target='_BLANK' href="http://kelas-coding.blogspot.com/"> Taofik Krisdiyanto</a>.</strong> All rights reserved. 
<!-- #Footer -->
<div class="modal fade" id="pos" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan
                            vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper.
                            Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus
                            nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla.
                            Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>