<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  TAMBAH DATA MODUL WEBSITE
              </h2>
          </div>
          <div class="body table-responsive">
            <?php
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/tambah_manajemenmodul',$attributes); 
          echo "
            <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value=''>
                    <tr>
                      <th width='120px' scope='row'>Nama Modul</th>  
                        <td><input type='text' class='form-control' name='a' required></td>
                    </tr>
                    <tr>
                      <th scope='row'>Link</th>              
                      <td style='color:red'><b>".base_url()."administrator/</b> 
                        <input type='text' class='form-control' name='b' style='width:50%; display:inline-block' required>
                      </td>
                    </tr>
                    <tr>
                      <th scope='row'>Publish</th>                
                      <td>
                          <input name='c' type='radio' id='yap' value='Y' class='with-gap radio-col-deep-purple' />
                          <label for='yap'>Ya</label>
                          <input name='c' type='radio' id='tap' value='N' class='with-gap radio-col-deep-purple' />
                          <label for='tap'>Tidak</label>
                      </td>
                    </tr>
                    <tr>
                      <th scope='row'>Aktif</th>                
                        <td>
                          <input name='d' type='radio' id='yaa' value='Y' class='with-gap radio-col-deep-purple' />
                          <label for='yaa'>Ya</label>
                          <input name='d' type='radio' id='taa' value='N' class='with-gap radio-col-deep-purple' />
                          <label for='taa'>Tidak</label>
                      </td>
                    </tr>
                  </tbody>
                  </table>
                
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Tambah</button>
                    <a href='".base_url().$this->uri->segment(1)."/manajemenmodul'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
              </div>
            "
            ;
            echo form_close();
            ?>
          </div>
      </div>
  </div>
</div>