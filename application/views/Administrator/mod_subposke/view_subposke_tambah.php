<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  TAMBAH DATA SUB KEGIATAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/tambah_subposke',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id_kegiatan' value=''>
                        <tr>
                          <th>Kode Rek 1</th>  
                          <td><input type='text' name='kd_rek3' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Sub Pos</th>
                          <td>
                            <select class='form-control show-tick' name='id_subpos' >
                            <option value='' selected>- Pilih Kategori -</option>";
                              foreach ($rows as $row){
                              echo "<option value='$row[id_subpos]'>$row[kode_pos].$row[kode_subpos] - $row[nama_subpos]</option>";
                              }
                              
                            echo "
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Rek 2</th>  
                          <td><input type='text' name='poske' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_kegiatan' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Keterangan</th>  
                          <td><textarea class='form-control' name='detail_anggaran' style='height:80px'></textarea></td>
                        </tr>
                        <tr>
                          <th>Jumlah (Rp.)</th>  
                          <td><input type='number' name='nilai_anggaran' class='form-control' placeholder='Ex. 100000'></td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Tambah</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemensubposke'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>