<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  EDIT DATA SUB KEGIATAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/edit_subposke',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id' value='$rows[id_kegiatan]'>
                        <tr>
                          <th>Kode Rek 1</th>  
                          <td><input type='text' name='kd_rek3' class='form-control' value='$rows[kd_rek3]'></td>
                        </tr>
                        <tr>
                          <th>Sub Pos</th>
                          <td>
                            <select class='form-control show-tick' name='id_subpos' >
                            <option value='' selected>- Pilih Kategori -</option>";
                              foreach ($record as $row){
                                if ($rows['id_subpos']==$row['id_subpos']){
                                    echo "<option value='$row[id_subpos]' selected>$row[kode_pos].$row[kode_subpos] - $row[nama_subpos]</option>";
                                    }else{
                                      echo "<option value='$row[id_subpos]'>$row[kode_pos].$row[kode_subpos] - $row[nama_subpos]</option>";
                                    }
                                  }
                              
                            echo "
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Kegiatan</th>  
                          <td><input type='text' name='poske' class='form-control' value='$rows[poske]'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_kegiatan' class='form-control' value='$rows[nama_kegiatan]'></td>
                        </tr>
                        <tr>
                          <th>Detail Kegiatan</th>  
                          <td><textarea class='form-control' name='detail_anggaran' style='height:80px'>$rows[detail_kegiatan]</textarea></td>
                        </tr>
                        <tr>
                          <th>Dana Anggaran</th>  
                          <td><input type='text' name='nilai_anggaran' class='form-control' value='$rows[nilai_anggaran]'></td>
                        </tr>
                        <tr>
                          <th>Tahun Akademik</th>
                          <td>
                            <select class='form-control show-tick' name='id_tahun' >
                            <option value='' selected>- Pilih Kategori -</option>";
                              foreach ($tahun as $row){
                                if ($rows['id_tahun']==$row['id_tahun']){
                                    echo "<option value='$row[id_tahun]' selected>$row[nama_tahun]</option>";
                                    }else{
                                      echo "<option value='$row[id_tahun]'>$row[nama_tahun]</option>";
                                    }
                                  }
                              
                            echo "
                            </select>
                          </td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Update</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemensubposke'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>