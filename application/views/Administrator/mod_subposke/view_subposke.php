<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA SUB KEGIATAN
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a class='pull-right btn btn-primary' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_subposke'>Tambah Data</a>
                  </li>
              </ul>
          </div>
          <div class="body table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable js-basic-example dataTable">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>KD Rek 1</th>
                          <th>KD Rek 2</th>
                          <th>Uraian</th>
                          <th>Anggaran</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $no = 1;
                        foreach ($record as $row)
                        { 
                        $a = $row['nilai_anggaran'];
                        $b = $row['total'];
                        $c = number_format($a,2,",",".");
                        $d = number_format($b,2,",",".");
                         echo "<tr><td>$no.</td>
                              <td>$row[kd_rek3]</td>
                              <td>$row[kode_pos].$row[kode_subpos].$row[poske]</td>
                              <td>$row[nama_kegiatan]</td>
                              <td>Rp. $c</td>
                              <td><center>
                                <a class='btn btn-success' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_subposke/$row[id_kegiatan]'><span class='glyphicon glyphicon-edit'></span> Edit</a>
                                <a class='btn btn-danger' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_subposke/$row[id_kegiatan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span> Hapus</a>
                              </center></td>
                          </tr>";
                          $no++;
                        }
                      ?>
                  </tbody>
              </table>
          </div>
              </div>
      </div>
  </div>
</div>