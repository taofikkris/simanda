<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?php echo $title; ?></title>
  <meta name="author" content="Taofik Krisdiyanto - Teknik Informatika Universitas Janabadra">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.png" type="image/png">
  <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
  <script src="<?php echo base_url(); ?>assets/login/plugin/sweetalert/js/sweetalert.min.js"></script>
  <script type="text/javascript">
    function nospaces(t){
        if(t.value.match(/\s/g)){
            alert('Maaf, Password Tidak Boleh Menggunakan Spasi,..');
            t.value=t.value.replace(/\s/g,'');
        }
    }
  </script>
    
</head>
<body class="login-page">
    <div class="login-box">
        <div class="logo text-center">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" height="80px">
            <a href="javascript:void(0);"><b>E-Budgeting</b></a>
            <small><font style="font-size: 15px;">Fakultas Teknik Universitas Janabadra</font></small>
        </div>
        <div class="card">
            <div class="body">
      <?php 
        if ($this->input->post('id_session')!=''){
        echo "<div class='alert alert-warning'><center>$title</center></div>";
      }
    ?>
                <form action="<?php echo site_url('administrator/reset_password'); ?>" method="post">
                    <div class="msg"></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name='a' placeholder="Input Password Baru" onkeyup="nospaces(this)" required="" autofocus="">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name='b' placeholder="Ulangi sekali lagi" onkeyup="nospaces(this)" required="" autofocus="">
                            <input type="hidden" name='id_session' value='<?php echo $this->session->id_session; ?>'>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <a href="<?=base_url().'administrator' ?>">Login</a>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" name ="lupa" class="btn btn-block btn-pink waves-effect">Kirimkan Permintaan</button>
                            <!-- <button type="submit" name ="submit" class="btn btn-block bg-pink waves-effect">LOGIN</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="footer text-center">
            <strong style="color: white;"> 18330059 &copy; 2019. All Right Reserved </strong>
        </div>
    </div>

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
   <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/pages/examples/sign-in.js"></script>
</body>
</html>
<!-- <!DOCTYPE html>
<html class="bg-black">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?php echo $title; ?></title>
  <meta name="author" content="BPC Amikom Yogyakarta">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/kc.png" type="image/png">
  <link href="<?php echo base_url(); ?>assets/login/plugin/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/plugin/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/app/css/app-login-v1.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/plugin/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css">
  <script src="<?php echo base_url(); ?>assets/login/plugin/sweetalert/js/sweetalert.min.js"></script>
  <script type="text/javascript">
    function nospaces(t){
        if(t.value.match(/\s/g)){
            alert('Maaf, Password Tidak Boleh Menggunakan Spasi,..');
            t.value=t.value.replace(/\s/g,'');
        }
    }
  </script>
</head>
<body>

<div class="container animated fadeIn">
  <div class="card card-container">
    <img id="profile-img" class="profile-img-card" src="<?php echo base_url(); ?>assets/images/logo.png">
    <p id="profile-name" class="judul">Reset Password</p>
    <hr>
    <p class="text-center pb10">Silahkan isi form dibawah ini</p>
    <br><br>
    <?php 
        if ($this->input->post('id_session')!=''){
        echo "<div class='alert alert-warning'><center>$title</center></div>";
      }
    ?>
    <form action="<?php echo site_url('administrator/reset_password'); ?>" class="form-signin" role="form" method="post" accept-charset="utf-8">
      <div class="password">
        <input type="password" class="form-control" name='a' placeholder="Input Password Baru" onkeyup="nospaces(this)" required="" autofocus="">
      </div>
      <div class="password2">
        <input type="password" class="form-control" name='b' placeholder="Ulangi sekali lagi" onkeyup="nospaces(this)" required="" autofocus="">
         <input type="hidden" name='id_session' value='<?php echo $this->session->id_session; ?>'>
      </div>
      <button type="submit" name ="lupa" class="btn btn-primary btn-signin">Kirimkan Permintaan</button>
    </form>
    <a href="<?php echo site_url('administrator'); ?>"> Login Administrator?</a>
  </div>
</div>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

</body>
</html> -->