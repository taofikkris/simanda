<html>
<head>
<title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo.png">
    <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
</head>
<!-- <body onload="window.print()"> -->
<div class="col-xs-12">
  <div style="text-align:justify; margin-top: 20px">
    <img src="<?php echo base_url(); ?>assets/images/logo.png" style="width: 80px; height: 80px; float:left; margin:0 8px 4px 0;"/>
    <p style="text-align: center; line-height: 20px">
      <span style="font-size: 15px"><strong>FAKULTAS TEKNIK</strong></span><br/>
      <span style="font-size: 20px;"><strong>UNIVERSITAS JANABADRA</strong></span><br/>
      <span style="font-size: 12px">Jl. Tentara Rakyat Mataram 55-57, Yogyakarta 55231 Telepon : (0274) 561039 Fax : (0274) 517251</span><br/>
      <span style="font-size: 12px">Website : www.teknik.janabadra.ac.id dan Email : teknik@janabadra.ac.id</span>
    </p>
  </div>
  <div style="clear:both"></div><br/>
  <hr style="border: 2px groove #000000;margin-top: -2px; width:100%"/>
  <hr style="border: 1px groove #000000; margin-top: -19px; width:100%"/>
</div>
<div class="col-xs-12">
  <h3>PENGELUARAN KEUANGAN</h3>
  <table class="table table-bordered table-striped">
    <thead>
                  <tr>
                      <th>No</th>
                      <th>Keperluan</th>
                      <th>Tanggal</th>
                      <th>Jumlah</th>
                  </tr>
              </thead>
              <tbody>
                 <?php
                    $no = 1;
                    foreach ($tampil_data1->result_array() as $rows1) {
                      $tanggal = tgl_indo($rows1['tgl']);
                      $dwet = $rows1['nilai_keluar'];
                      $arto = number_format($dwet,2,",",".");
                  ?>
                  <tr>
                      <th scope="row"><?php echo $no ?>.</th>
                      <td><?php echo $rows1['nama_kegiatan'] ?> (<?php echo $rows1['nama_kegiatan'] ?>)</td>
                      <td><?php echo $tanggal ?></td>
                      <td>Rp. <?php echo $arto ?></td>
                  </tr>
                  <?php $no++; } ?>
              </tbody>
  </table>
</div>

  <!-- Jquery Core Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

  <!-- Bootstrap Core Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>
</body>
</html>