<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA SUB POS KEGIATAN NOMENKLATUR PENGELUARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                      <tr>
                          <th>Kode Pos</th>
                          <th>Anggaran</th>
                          <th>Anggaran</th>
                          <th>Realisasi</th>
                          <th>Saldo</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        $tahun=$this->session->tahun;
                        $a = $this->db->query("SELECT  pos.id_pos,pos.kode_pos,pos.nama_pos,sum(subkegiatan.nilai_anggaran) as anggaran FROM pos JOIN subpos ON pos.id_pos=subpos.id_pos JOIN subkegiatan ON subpos.id_subpos=subkegiatan.id_subpos WHERE subkegiatan.id_tahun='".$tahun."' GROUP BY pos.id_pos")->result_array(); 
                        $no = 1;
                        foreach ($a as $c)
                        { 
                          $ab = $this->db->query("SELECT SUM(nilai_keluar) as metu FROM `realisasi` WHERE id_pos='".$c[id_pos]."' and tahun='".$tahun."'")->result_array();
                          foreach ($ab as $cc) {
                            $f = $c['anggaran'];
                            $g = $cc['metu'];
                            $h = number_format($f,2,",",".");
                            $i = number_format($g,2,",",".");
                            $e = $c['anggaran']-$cc['metu'];
                            $f = number_format($e,2,",",".");
                             echo "<td>$c[kode_pos]</td>
                                  <td>$c[nama_pos]</td>
                                  <td></td>
                                  <td>Rp. $i</td>
                                  <td>Rp. $f</td>
                              </tr>";
                              $no++;
                        }
                            
                        }
                      ?>
                  </tbody>
              </table>
          </div>
        </div>
      </div>
  </div>
</div>