<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA KEUANGAN
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_keuangan'>Tambahkan Data</a>
                  </li>
              </ul>
          </div>
          <div class="body">
                      <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <?php 
                        $no = 1;
                        foreach ($record as $row){
                         echo "
                              <li role='presentation' class='active'><a href='#hasil' data-toggle='tab'>$row[nama_tahun]</a></li>
                              ";
                          $no++;
                        }
                      ?>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane animated flipInX active" id="hasil">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Pos</th>
                                      <th>Nama Pos</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tfoot>
                                <?php 
                        $no = 1;
                        foreach ($pos as $row){
                         echo "<tr><td>$no.</td>
                              <td>$row[kode_pos]</td>
                              <td>$row[nama_pos]</td>
                              <td><center>
                                <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_pos/$row[id_pos]'><span class='glyphicon glyphicon-edit'></span></a>
                                <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_pos/$row[id_pos]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                              </center></td>
                          </tr>";
                          $no++;
                        }
                      ?>
                              </tbody>
                            </table>
                          </div>



                          <!-- <div role="tabpanel" class="tab-pane animated flipInX" id="keluar">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Tanggal</th>
                                      <th>Keperluan</th>
                                      <th>Jumlah</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tfoot>
                                <?php
                                  $metu = $this->db->query("SELECT status , SUM(jumlah) AS keluar FROM keuangan WHERE status = 'keluar'")->result_array();
                                  foreach ($metu as $anu1) {
                                    $a1 = $anu1['keluar'];
                                    $b1 = number_format($a1,2,",",".");
                                    echo "<tr>
                                      <th colspan='3'>Jumlah</th>
                                      <th colspan='2'>Rp. $b1</th>   
                                    </tr>";
                                  }
                                ?>
                              </tfoot>
                              <tbody>
                                <?php 
                                  $no = 1;
                                  foreach ($keluar as $row1){
                                    $tanggal = tgl_indo($row1['tgl']);
                                    $angka = $row1['jumlah'];
                                    $uang = number_format($angka,2,",",".");

                                  echo "<tr><td>$no</td>
                                            <td>$tanggal</td>
                                            <td>$row1[tujuan]</td>
                                            <td>Rp. $uang</td>
                                            <td><center>
                                              <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_keuangan/$row1[id_keuangan]'><span class='glyphicon glyphicon-edit'></span></a>
                                              <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_keuangan/$row1[id_keuangan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                                            </center></td>
                                        </tr>";
                                    $no++;
                                  }
                                ?>
                              </tbody>
                            </table>
                          </div> -->
                      </div>
                  </div>
              </div>
      </div>
  </div>
</div>