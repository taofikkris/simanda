<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  TAMBAH DATA POS ANGGARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/tambah_pos',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id_pos' value=''>
                        <tr>
                          <th>Kode Rekening 1</th>  
                          <td><input type='text' name='kd_rek1' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Kode Rekening 2</th>  
                          <td><input type='text' name='kode_pos' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_pos' class='form-control'></td>
                        </tr>

                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Simpan</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemenpos'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>