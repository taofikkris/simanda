<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  EDIT DATA POS ANGGARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/edit_pos',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id' value='$rows[id_pos]'>
                        <tr>
                          <th>Kode Rek 1</th>  
                          <td><input type='text' name='kd_rek1' class='form-control' value='$rows[kd_rek1]'></td>
                        </tr>
                        <tr>
                          <th>Kode Rek 2</th>  
                          <td><input type='text' name='kode_pos' class='form-control' value='$rows[kode_pos]'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_pos' class='form-control' value='$rows[nama_pos]'></td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Update</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemenpos'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>