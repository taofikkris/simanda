
          <div class="body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <div class="header bg-light-blue">
                    <h2>
                        RENCANA ANGGARAN
                    </h2>
                </div>
                <div class="body table-responsive">
                    <div class="demo-single-button-dropdowns">
                         <?php
                            $tahun=$this->session->tahun;
                            $query = $this->db->query("SELECT SUM(nilai_anggaran) AS total  FROM `subkegiatan` WHERE id_tahun='".$tahun."'");
                            foreach ($query->result_array() as $rows) {
                              $dwet = $rows['total'];
                              $arto = number_format($dwet,2,",",".");
                               echo "<center><h2>Rp. $arto</h2></center>";
                             } 
                         ?>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <div class="header bg-light-green">
                    <h2>
                       REALISASI
                    </h2>
                </div>
                <div class="body table-responsive">
                    <div class="demo-single-button-dropdowns">
                         <?php
                            $tahun=$this->session->tahun;
                            $query = $this->db->query("SELECT SUM(nilai_keluar) AS nilai_keluar  FROM `realisasi` WHERE tahun='".$tahun."'");

                            foreach ($query->result_array() as $rows) {
                              $dwet = $rows['nilai_keluar'];
                              $arto = number_format($dwet,2,",",".");
                               echo "<center><h2>Rp. $arto</h2></center>";
                             } 
                         ?>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <div class="header bg-amber">
                    <h2>
                       SALDO
                    </h2>
                </div>
                <div class="body table-responsive">
                    <div class="demo-single-button-dropdowns">
                         <?php
                          $tahun=$this->session->tahun;
                            $a = $this->db->query("SELECT SUM(nilai_anggaran) AS total  FROM `subkegiatan` WHERE id_tahun='".$tahun."'")->result_array();
                            $b = $this->db->query("SELECT SUM(nilai_keluar) AS nilai_keluar  FROM `realisasi` WHERE tahun='".$tahun."'")->result_array();
                            foreach($a as $c) {
                                foreach($b as $d) {
                                    $dwet = $c['total'] - $d['nilai_keluar'];
                                    $arto = number_format($dwet,2,",",".");
                                     echo "<center><h2>Rp. $arto</h2></center>";
                                }
                            }
                         ?>
                      </div>
                </div>
              </div>
            </div>
          </div>
        