<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA SUB POS KEGIATAN NOMENKLATUR PENGELUARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Kode Rek 1</th>
                          <th>Kode Rek 2</th>
                          <th>Uraian</th>
                          <th>Anggaran</th>
                          <th>Realisasi</th>
                          <th>Saldo</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        $tahun=$this->session->tahun;
                        $a = $this->db->query("SELECT  subpos.id_subpos,subpos.kd_rek2,pos.id_pos,pos.kode_pos,subpos.kode_subpos,subpos.nama_subpos,sum(subkegiatan.nilai_anggaran) as anggaran FROM pos JOIN subpos ON pos.id_pos=subpos.id_pos JOIN subkegiatan ON subpos.id_subpos=subkegiatan.id_subpos WHERE subkegiatan.id_tahun='".$tahun."' GROUP BY subpos.id_subpos")->result_array();
                        $no = 1;
                        foreach ($a as $row)
                        { 
                          $ab = $this->db->query("SELECT SUM(nilai_keluar) as metu FROM `realisasi` WHERE id_subpos='".$row[id_subpos]."' and tahun='".$tahun."'")->result_array();
                          foreach ($ab as $ro) {
                              $a = $row['anggaran'];
                              $b = $ro['metu'];
                              $c = number_format($a,2,",",".");
                              $d = number_format($b,2,",",".");
                              $e = $row['anggaran']-$ro['metu'];
                              $f = number_format($e,2,",",".");
                               echo "<tr><td>$no.</td>
                                    <td>$row[kd_rek2]</td>
                                    <td>$row[kode_pos].$row[kode_subpos]</td>
                                    <td>$row[nama_subpos]</td>
                                    <td>Rp. $c</td>
                                    <td>Rp. $d</td>
                                    <td>Rp. $f</td>
                                </tr>";
                                $no++;
                          }
                        }
                      ?>
                  </tbody>
              </table>
          </div>
        </div>
      </div>
  </div>
</div>