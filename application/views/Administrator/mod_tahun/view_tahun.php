<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA TAHUN
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_tahun'>Tambah Data</a>
                  </li>
              </ul>
          </div>
          <div class="body table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable js-basic-example dataTable" align="center">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Tahun Akademik</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $no = 1;
                        foreach ($record as $row){
                         echo "<tr><td>$no.</td>
                              <td>$row[nama_tahun]</td>
                              <td><center>
                                <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_tahun/$row[id_tahun]'><span class='glyphicon glyphicon-edit'></span></a>
                                <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_tahun/$row[id_tahun]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                              </center></td>
                          </tr>";
                          $no++;
                        }
                      ?>
                  </tbody>
              </table>
          </div>
              </div>
      </div>
  </div>
</div>