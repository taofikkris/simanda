<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  EDIT DATA TAHUN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/edit_tahun',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id' value='$rows[id_tahun]'>
                        <tr>
                          <th>Tahun Akademik</th>  
                          <td><input type='text' name='nama_tahun' class='form-control' value='$rows[nama_tahun]'></td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Update</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajementahun'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>