<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  TAMBAH DATA SUB POS ANGGARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/tambah_subpos',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id_subpos' value=''>
                        <tr>
                          <th>Kode Rek 1</th>  
                          <td><input type='text' name='kd_rek2' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Pos</th>
                          <td>
                            <select class='form-control show-tick' name='id_pos' >
                            <option value='' selected>- Pilih Kategori -</option>";
                              foreach ($record as $row){
                              echo "<option value='$row[id_pos]'>$row[kode_pos] - $row[nama_pos]</option>";
                              }
                            echo "
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Rek 2</th>  
                          <td><input type='text' name='kode_subpos' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_subpos' class='form-control'></td>
                        </tr>

                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Simpan</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemensubpos'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>


      </div>
  </div>
</div>