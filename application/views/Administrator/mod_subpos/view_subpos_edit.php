<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  EDIT DATA SUB POS ANGGARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/edit_subpos',$attributes); 
                  echo "<table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id' value='$rows[id_subpos]'>
                        <tr>
                          <th>Kode Rek 1</th>  
                          <td><input type='text' name='kd_rek2' class='form-control' value='$rows[kd_rek2]'></td>
                        </tr>
                        <tr>
                          <th>Pos</th>
                          <td>
                            <select class='form-control show-tick' name='id_pos' >
                            <option value='' selected>- Pilih Kategori -</option>";
                              foreach ($record as $row){
                                if ($rows['id_pos']==$row['id_pos']){
                                    echo "<option value='$row[id_pos]' selected>$row[kode_pos] - $row[nama_pos]</option>";
                                    }else{
                                      echo "<option value='$row[id_pos]'>$row[kode_pos] - $row[nama_pos]</option>";
                                    }
                                  }
                              
                            echo "
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Rek 2</th>  
                          <td><input type='text' name='kode_subpos' class='form-control' value='$rows[kode_subpos]'></td>
                        </tr>
                        <tr>
                          <th>Uraian</th>  
                          <td><input type='text' name='nama_subpos' class='form-control' value='$rows[nama_subpos]'></td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer pull-right'>
                        <button type='submit' name='submit' class='btn btn-primary'>Update</button>
                        <a href='".base_url().$this->uri->segment(1)."/manajemensubpos'><button type='button' class='btn btn-danger'>Cancel</button></a>
                        
                      </div>";
                echo form_close();
              ?>

          </div>
      </div>
  </div>
</div>