<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  DATA SUB POS ANGGARAN
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a class='pull-right btn btn-primary' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_subpos'>Tambah Data</a>
                  </li>
              </ul>
          </div>
          <div class="body table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable js-basic-example dataTable">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>KD Rek 1</th>
                          <th>KD Rek 2</th>
                          <th>Uraian</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $no = 1;
                        foreach ($record as $row) {
                         echo "<tr><td>$no.</td>
                              <td>$row[kd_rek2]</td>
                              <td>$row[kode_pos].$row[kode_subpos]</td>
                              <td>$row[nama_subpos]</td>
                              <td><center>
                                <a class='btn btn-success' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_subpos/$row[id_subpos]'><span class='glyphicon glyphicon-edit'></span> Edit</a>
                                <a class='btn btn-danger' title='Hapus Data' href='".base_url().$this->uri->segment(1)."/delete_subpos/$row[id_subpos]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span> Hapus</a>
                              </center></td>
                          </tr>";
                          $no++;
                        }
                      ?>
                  </tbody>
              </table>
          </div>
              </div>
      </div>
  </div>
</div>