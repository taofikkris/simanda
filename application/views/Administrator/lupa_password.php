<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?php echo $title; ?></title>
  <meta name="author" content="Taofik Krisdiyanto - Teknik Informatika Universitas Janabadra">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.png" type="image/png">
  <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/login/app/css/app-login-v1.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
    
</head>
<body class="fp-page">
    <div class="fp-box">
       
        <div class="card">
            <div class="logo text-center">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" height="100px">
            <a style="color: #555;"><b>Simanda</b></a>
            <small><font style="font-size: 16px; color: #555;">Pemerintahan Desa Ngalang</font></small>
            </div>
            <div class="body">
                <form action="<?php echo site_url('administrator/lupapassword'); ?>" class="form-signin" role="form" method="post" accept-charset="utf-8">
                    <div class="msg">
                        Masukan Email anda, Kami akan mengirimkan Konfirmasi Reset Password melalui email tersebut.
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                        </div>
                    </div>

                    <button type="submit" name ="lupa" class="btn btn-block btn-lg bg-pink waves-effect">Kirimkan Permintaan</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="<?php echo site_url('administrator'); ?>"> Login Kembali </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="footer text-center">
            <strong style="color: white;"> Taofik Krisdiyanto &copy; 2019. All Right Reserved </strong>
        </div>
    </div>

   <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/pages/examples/sign-in.js"></script>
</body>
</html>