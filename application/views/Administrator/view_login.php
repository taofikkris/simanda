<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $title; ?></title>
	<meta name="author" content="Taofik Krisdiyanto - Teknik Informatika Universitas Janabadra">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.png" type="image/png">
	<link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/login/app/css/app-login-v1.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    
</head>
<body class="login-page">
    <div class="login-box">
        <div class="card">
            <div class="logo text-center">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" height="100px">
            <a style="color: #555;"><b>Simanda</b></a>
            <small><font style="font-size: 16px; color: #555;">Pemerintahan Desa Ngalang</font></small>
            </div>
            <div class="body">
			<?php 
	          if ($this->input->post('email')!=''){
	            echo "<div class='alert alert-warning'><center>$title</center></div>";
	          }elseif($this->input->post('a')!=''){
	            echo "<div class='alert alert-danger'><center>$title</center></div>";
	          }
	        ?>
                <form action="<?=base_url().'administrator/index' ?>" method="post">
                    
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="tahun" required>
                                        <option value="">-- Pilih Tahun Anggaran --</option>
                                        <?php 
                                            foreach ($record as $row){
                                             echo "
                                                <option value='$row[id_tahun]'>$row[nama_tahun]</option>
                                                ";
                                            }
                                          ?>
                                        
                                    </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <a href="<?=base_url().'administrator/lupapassword' ?>">Lupa Password?</a>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" name ="submit" class="btn btn-block bg-pink waves-effect">LOGIN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="footer text-center">
                <strong style="color: white;"> Taofik Krisdiyanto &copy; 2019. All Right Reserved </strong>
            </div>
    </div>


	 <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/pages/examples/sign-in.js"></script>
</body>
</html>