
    <tr>
                          <th>Kode Pos</th>
                          <td>
                            <select class='form-control show-tick' name='pos' id='pos'>
                            <option value='' selected>- Pilih Kategori -</option>";
                              <?php
                                foreach($pos as $data){ // Lakukan looping pada variabel siswa dari controller
                                  echo "<option value='".$data->id_pos."'>".$data->kode_pos." - ".$data->nama_pos."</option>";
                                }
                                ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Sub Pos</th>
                          <td>
                            <select name="subpos" id="subpos" style="width: 200px;">
                              <option value="">Pilih</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Kode Kegiatan</th>
                          <td>
                            <select name="subke" id="subke" style="width: 200px;">
                              <option value="">Pilih</option>
                            </select>
                          </td>
                        </tr>
  
  <!-- Load librari/plugin jquery nya -->
  <script src="<?php echo base_url("js/jquery.min.js"); ?>" type="text/javascript"></script>
  
  <script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading").hide();
    
    $("#pos").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#subpos").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("index.php/administrator/listPos"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_pos : $("#pos").val()}, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya

          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#subpos").html(response.list_subpos).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

  <script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#load").hide();
    
    $("#subpos").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#subke").hide(); // Sembunyikan dulu combobox kota nya
      $("#load").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("index.php/administrator/listSubpos"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_subpos : $("#subpos").val()}, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#load").hide(); // Sembunyikan loadingnya

          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#subke").html(response.list_subke).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>
</body>
</html>

