<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  TAMBAH DATA REALISASI ANGGARAN
              </h2>
          </div>
          <div class="body table-responsive">
              <?php

                $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart($this->uri->segment(1).'/tambah_realisasi',$attributes); ?>
                  <table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type='hidden' name='id_realisasi' value=''>
                         <tr>
                          <th width='120px' scope='row'>Pos</th>  
                          <td>
                            <?php
                            $style_pos='class="form-control input-sm" id="pos_id" onChange="tampilSubpos()"';
                            echo form_dropdown('pos_id',$provinsi,'',$style_pos);
                            ?>
                          </td>
                        </tr>

                        <tr>
                          <th width='120px' scope='row'>Sub Pos</th>  
                          <td>
                            <?php
                            $style_subpos='class="form-control input-sm" id="subpos_id" onChange="tampilSubkegiatan()"';
                            echo form_dropdown("subpos_id",array('Pilih Subpos'=>'- Pilih Subpos -'),'',$style_subpos);
                            ?>
                          </td>
                        </tr>

                         <tr>
                          <th width='120px' scope='row'>Sub Kegiatan</th>  
                          <td>
                            <?php
                            $style_subkegiatan='class="form-control input-sm" id="subkegiatan_id"';
                            echo form_dropdown("subkegiatan_id",array('Pilih Sub Kegiatan'=>'- Pilih Sub Kegiatan -'),'',$style_subkegiatan);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <th width='120px' scope='row'>Tanggal</th>  
                          <td><input type='date' name='tgl' class='datepickr form-control' placeholder='Masukan Tanggal...'></td>
                        </tr>
                        <tr>
                          <th>Detail Kegiatan</th>  
                          <td><input type='text' name='detail_kegiatan' class='form-control'></td>
                        </tr>
                        <tr>
                          <th>Jumlah (Rp.)</th>  
                          <td><input type='number' name='nilai_keluar' class='form-control' placeholder='Ex. 100000'></td>
                        </tr>
                        <tr>
                          <th scope='row'>Upload Nota</th>              
                          <td><input type='file' class='form-control' name='nota'></td>
                        </tr>
                      </tbody>
                      </table>
                  
                  <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Tambah</button>
                        <a href='".base_url().$this->uri->segment(1)."/realisasi'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                        
                      </div>
                      <?php echo form_close(); ?>
          </div>
      </div>
  </div>
</div>