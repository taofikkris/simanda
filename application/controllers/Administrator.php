<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Administrator extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Model_app','',TRUE);  
    }

    public function pilih_subpos(){
        $data['subpos']=$this->Model_app->ambil_subpos($this->uri->segment(3));
        $this->load->view('administrator/drop_subpos',$data);
    }

    public function pilih_subkegiatan(){
        $data['subkegiatan']=$this->Model_app->ambil_subkegiatan($this->uri->segment(3));
        $this->load->view('administrator/drop_subkegiatan',$data);
    }

	function index(){
		if (isset($_POST['submit'])){
			$username = $this->input->post('username');
			$password = sha1($this->input->post('password'));
            $tahun = $this->input->post('tahun');
			$cek = $this->Model_app->cek_login($username,$password,'users');
		    $row = $cek->row_array();
		    $total = $cek->num_rows();
			if ($total > 0){
				$this->session->set_userdata('upload_image_file_manager',true);
				$this->session->set_userdata(array('username'=>$row['username'],
								   'level'=>$row['level'],
                                   'tahun'=>$tahun,
                                   'id_session'=>$row['id_session']));

				redirect($this->uri->segment(1).'/home');
			}else{
				redirect('administrator/gagallogin'); 
			}
		}else{
			$data['title'] = 'Administrator &rsaquo; Log In';
            $data['record'] = $this->Model_app->view_ordering('tahun','id_tahun','ASC'); 
			$this->load->view('administrator/view_login',$data);
		}
	}

    function gagallogin(){
        $url=base_url('administrator');
        redirect($url);
    }

    //Controller resetpassword
	function reset_password(){
        if (isset($_POST['submit'])){
            $usr = $this->Model_app->edit('users', array('id_session' => $this->input->post('id_session')));
            if ($usr->num_rows()>=1){
                if ($this->input->post('a')==$this->input->post('b')){
                    $data = array('password'=>sha1($this->input->post('a')));
                    $where = array('id_session' => $this->input->post('id_session'));
                    $this->Model_app->update('users', $data, $where);

                    $row = $usr->row_array();
                    $this->session->set_userdata('upload_image_file_manager',true);
                    $this->session->set_userdata(array('username'=>$row['username'],
                                       'level'=>$row['level'],
                                       'id_session'=>$row['id_session']));
                    redirect('administrator/home');
                }else{
                    $data['title'] = 'Password Tidak sama!';
                    $this->load->view('administrator/view_reset',$data);
                }
            }else{
                $data['title'] = 'Terjadi Kesalahan!';
                $this->load->view('administrator/view_reset',$data);
            }
        }else{
            $this->session->set_userdata(array('id_session'=>$this->uri->segment(3)));
            $data['title'] = 'Reset Password';
            $this->load->view('administrator/view_reset',$data);
        }
    }

    //Controller Reset Password
    function lupapassword(){
        if (isset($_POST['lupa'])){
            $email = strip_tags($this->input->post('email'));
            $cekemail = $this->Model_app->edit('users', array('email' => $email))->num_rows();
            if ($cekemail <= 0){
                $data['title'] = 'Alamat email tidak ditemukan';
                $this->load->view('administrator/view_login',$data);
            }else{
                $iden = $this->Model_app->edit('identitas', array('id_identitas' => 1))->row_array();
                $usr = $this->Model_app->edit('users', array('email' => $email))->row_array();
                $this->load->library('email');

                $tgl = date("d-m-Y H:i:s");
                $subject      = 'Lupa Password ...';
                $message      = "<html><body>
                                    <table style='margin-left:25px'>
                                        <tr><td>Halo $usr[nama_lengkap],<br>
                                        Seseorang baru saja meminta untuk mengatur ulang kata sandi Anda di <span style='color:red'>$iden[url]</span>.<br>
                                        Klik di sini untuk mengganti kata sandi Anda.<br>
                                        Atau Anda dapat copas (Copy Paste) url dibawah ini ke address Bar Browser anda :<br>
                                        <a href='".base_url()."administrator/reset_password/$usr[id_session]'>".base_url()."administrator/reset_password/$usr[id_session]</a><br><br>

                                        Tidak meminta penggantian ini?<br>
                                        Jika Anda tidak meminta kata sandi baru, segera beri tahu kami.<br>
                                        Email. $iden[email], No Telp. $iden[no_telp]</td></tr>
                                    </table>
                                </body></html> \n";
                
                $this->email->from($iden['email'], $iden['nama_website']);
                $this->email->to($usr['email']);
                $this->email->cc('');
                $this->email->bcc('');

                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->set_mailtype("html");
                $this->email->send();
                
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $data['title'] = 'Password terkirim ke '.$usr['email'];
                $this->load->view('administrator/view_login',$data);
            }
        }else{
            $data['title'] = 'Reset Password Administrator';
			$this->load->view('administrator/lupa_password',$data);
        }
    }

    //Controller Home
    function home(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['cp'] = $this->Model_app->countpos('pos','tahun',''.$tahun.'');
        $data['csu'] = $this->Model_app->countsubpos('subpos','tahun',''.$tahun.'');
        $data['csub'] = $this->Model_app->countsubposke('subkegiatan','id_tahun',''.$tahun.'');
        $data['creal'] = $this->Model_app->countrealisasi('realisasi','tahun',''.$tahun.'');
    	$data['title'] = 'Halaman Dasboard Administrator';
        if ($this->session->level=='admin'){
          $this->template->load('administrator/template','administrator/view_home',$data);
        }else{
          $data['users'] = $this->Model_app->view_where('users',array('username'=>$this->session->username))->row_array();
          $data['modul'] = $this->Model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $data['record'] = $this->Model_app->view_ordering('tahun','username','ASC');
          $this->template->load('administrator/template','administrator/view_home',$data);
        }
    }

    function tahunhasil(){
        $this->cek_admin();
        $data['title'] = 'Manajemen View Tahun';
        $data['pos']=$this->Model_app->view_ordering('pos','username','ASC');
        $data['subpos']=$this->Model_app->view_ordering('subpos','username','ASC');
        $this->template->load('administrator/template','administrator/view_tahun',$data);
    }

    //Controller Identitas Website
    function identitaswebsite(){
        $this->cek_admin();
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'assets/images/';
            $config['allowed_types'] = 'gif|jpg|png|ico';
            $config['max_size'] = '500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('j');
            $hasil=$this->upload->data();

            if ($hasil['file_name']==''){
            	$data = array('nama_website'=>$this->db->escape_str($this->input->post('a')),
                                'email'=>$this->db->escape_str($this->input->post('b')),
                                'url'=>$this->db->escape_str($this->input->post('c')),
                                'facebook'=>$this->input->post('d'),
                                'twitter'=>$this->input->post('d1'),
                                'instagram'=>$this->input->post('d2'),
                                'google'=>$this->input->post('d3'),
                                'youtube'=>$this->input->post('d4'),
                                'alamat'=>$this->db->escape_str($this->input->post('e')),
                                'no_telp'=>$this->db->escape_str($this->input->post('f')),
                                'meta_deskripsi'=>$this->input->post('g'),
                                'meta_keyword'=>$this->db->escape_str($this->input->post('h')),
                                'maps'=>$this->input->post('i'));
            }else{
            	$data = array('nama_website'=>$this->db->escape_str($this->input->post('a')),
                                'email'=>$this->db->escape_str($this->input->post('b')),
                                'url'=>$this->db->escape_str($this->input->post('c')),
                               'facebook'=>$this->input->post('d'),
                                'twitter'=>$this->input->post('d1'),
                                'instagram'=>$this->input->post('d2'),
                                'google'=>$this->input->post('d3'),
                                'youtube'=>$this->input->post('d4'),
                                'alamat'=>$this->db->escape_str($this->input->post('e')),
                                'no_telp'=>$this->db->escape_str($this->input->post('f')),
                                'meta_deskripsi'=>$this->input->post('g'),
                                'meta_keyword'=>$this->db->escape_str($this->input->post('h')),
                                'favicon'=>$hasil['file_name'],
                                'maps'=>$this->input->post('i'));
            }
	    	$where = array('id_identitas' => $this->input->post('id'));
			$this->Model_app->update('identitas', $data, $where);

			redirect($this->uri->segment(1).'/identitaswebsite');
		}else{
			$proses = $this->Model_app->edit('identitas', array('id_identitas' => 1))->row_array();
			$data = array('record' => $proses);
			$data['title'] = 'Identitas Website';
			$this->template->load('administrator/template','administrator/mod_identitas/view_identitas',$data);
		}
	}

    // Controller Modul User
    function manajemenuser(){
        $this->cek_admin();
        $data['title'] = 'Data User';
        $data['record'] = $this->Model_app->view_ordering('users','username','DESC');
        $this->template->load('administrator/template','administrator/mod_users/view_users',$data);
    }

    function tambah_manajemenuser(){
        $this->cek_admin();
        $data['title'] = 'Tambah Data User';
        $id = $this->session->username;
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>sha1($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'level'=>$this->db->escape_str($this->input->post('g')),
                                    'blokir'=>'N',
                                    'id_session'=>date('YmdHis'));
            }else{
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>sha1($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'level'=>$this->db->escape_str($this->input->post('g')),
                                    'blokir'=>'N',
                                    'id_session'=>date('YmdHis'));
            }
            $this->Model_app->insert('users',$data);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              $sess = date('YmdHis');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$sess,
                              'id_modul'=>$modul[$i]);
                $this->Model_app->insert('users_modul',$datam);
              }

            redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('a'));
        }else{
            $proses = $this->Model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'admin'), 'id_modul','DESC');
            $data = array('record' => $proses);
            $this->template->load('administrator/template','administrator/mod_users/view_users_tambah',$data);
        }
    }

    function edit_manajemenuser(){
        $this->cek_admin();
        $data['title'] = 'Edit Data User';
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']=='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                   'password'=>sha1($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                  'password'=>sha1($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }
            $where = array('username' => $this->input->post('id'));
            $this->Model_app->update('users', $data, $where);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$this->input->post('ids'),
                              'id_modul'=>$modul[$i]);
                $this->Model_app->insert('users_modul',$datam);
              }

            redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('id'));
        }else{
            if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
                $proses = $this->Model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->Model_app->view_join_where_akses('users_modul','modul','id_modul', 'id_session',array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->Model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'admin'), 'id_modul','ASC');
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses);
                $this->template->load('administrator/template','administrator/mod_users/view_users_edit',$data);
            }else{
                redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->session->username);
            }
        }
    }

    function delete_manajemenuser(){
        $this->cek_admin();
        $id = array('username' => $this->uri->segment(3));
        $this->Model_app->delete('users',$id);
        redirect($this->uri->segment(1).'/manajemenuser');
    }

    function delete_akses(){
        $this->cek_admin();
        $id = array('id_umod' => $this->uri->segment(3));
        $this->Model_app->delete('users_modul',$id);
        redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->uri->segment(4));
    }

    // Controller Modul Modul

    function manajemenmodul(){
        $this->cek_admin();
        $data['title'] = 'Data Modul';
        if ($this->session->level=='admin'){
            $data['record'] = $this->Model_app->view_ordering('modul','id_modul','DESC');
        }else{
            $data['record'] = $this->Model_app->view_where_ordering('modul',array('username'=>$this->session->username),'id_modul','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_modul/view_modul',$data);
    }

    function tambah_manajemenmodul(){
        $this->cek_admin();
        $data['title'] = 'Tambah Modul';
        if (isset($_POST['submit'])){
            $data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
                        'username'=>$this->session->username,
                        'link'=>$this->db->escape_str($this->input->post('b')),
                        'static_content'=>'',
                        'gambar'=>'',
                        'publish'=>$this->db->escape_str($this->input->post('c')),
                        'status'=>'admin',
                        'aktif'=>$this->db->escape_str($this->input->post('d')),
                        'urutan'=>'0',
                        'link_seo'=>'');
            $this->Model_app->insert('modul',$data);
            redirect($this->uri->segment(1).'/manajemenmodul');
        }else{
            $this->template->load('administrator/template','administrator/mod_modul/view_modul_tambah');
        }
    }

    function edit_manajemenmodul(){
        $this->cek_admin();
        $data['title'] = 'Edit Modul';
        if (isset($_POST['submit'])){
            $data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
                        'username'=>$this->session->username,
                        'link'=>$this->db->escape_str($this->input->post('b')),
                        'static_content'=>'',
                        'gambar'=>'',
                        'publish'=>$this->db->escape_str($this->input->post('c')),
                        'status'=>$this->db->escape_str($this->input->post('e')),
                        'aktif'=>$this->db->escape_str($this->input->post('d')),
                        'urutan'=>'0',
                        'link_seo'=>'');
            $where = array('id_modul' => $this->input->post('id'));
            $this->Model_app->update('modul', $data, $where);
            redirect($this->uri->segment(1).'/manajemenmodul');
        }else{
            if ($this->session->level=='admin'){
                 $proses = $this->Model_app->edit('modul', array('id_modul' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('modul', array('id_modul' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_modul/view_modul_edit',$data);
        }
    }

    function delete_manajemenmodul(){
        $this->cek_admin();
        if ($this->session->level=='admin'){
            $id = array('id_modul' => $this->uri->segment(3));
        }else{
            $id = array('id_modul' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('modul',$id);
        redirect($this->uri->segment(1).'/manajemenmodul');
    }

    // Controller Modul Tahun
    function manajementahun(){
        $this->cek_admin();
        $data['title'] = 'Manajemen Tahun Akademik';
        $data['record'] = $this->Model_app->view_ordering('tahun','username','ASC');
        $this->template->load('administrator/template','administrator/mod_tahun/view_tahun',$data);
    }

    function tambah_tahun(){
        $this->cek_admin();
        $data['title'] = 'Tambah Manajemen Tahun Akdemik';
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'nama_tahun'=>$this->db->escape_str($this->input->post('nama_tahun'))
            );
            $this->Model_app->insert('tahun',$data);
            redirect($this->uri->segment(1).'/manajementahun');
        }else{
            $this->template->load('administrator/template','administrator/mod_tahun/view_tahun_tambah', $data);
        }
    }

    function edit_tahun(){
        $this->cek_admin();
        $data['title'] = 'Edit Manajemen Tahun Akdemik';
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'nama_tahun'=>$this->db->escape_str($this->input->post('nama_tahun'))
            );
            $where = array('id_tahun' => $this->input->post('id'));
            $this->Model_app->update('tahun', $data, $where);
            redirect($this->uri->segment(1).'/manajementahun');
        }else{
            $id = $this->uri->segment(3);
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
                 $proses = $this->Model_app->edit('tahun', array('id_tahun' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('tahun', array('id_tahun' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_tahun/view_tahun_edit',$data);
        }
    }

    function delete_tahun(){
        $this->cek_admin();
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $id = array('id_tahun' => $this->uri->segment(3));
        }else{
            $id = array('id_tahun' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('tahun',$id);
        redirect($this->uri->segment(1).'/manajementahun');
    }

    // Controller Modul Pos
    function manajemenpos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Manajemen Pos';
        $data['record'] = $this->Model_app->view_where_likek('pos',''.$tahun.'','tahun');
        $this->template->load('administrator/template','administrator/mod_pos/view_pos',$data);
    }

    function tambah_pos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Tambah Manajemen Pos';
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'kd_rek1'=>$this->db->escape_str($this->input->post('kd_rek1')),
                        'kode_pos'=>$this->db->escape_str($this->input->post('kode_pos')),
                        'nama_pos'=>$this->db->escape_str($this->input->post('nama_pos')),
                        'tahun'=>''.$tahun.''
            );
            $this->Model_app->insert('pos',$data);
            redirect($this->uri->segment(1).'/manajemenpos');
        }else{
            $this->template->load('administrator/template','administrator/mod_pos/view_pos_tambah', $data);
        }
    }

    function edit_pos(){
        $this->cek_admin();
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'kd_rek1'=>$this->db->escape_str($this->input->post('kd_rek1')),
                        'kode_pos'=>$this->db->escape_str($this->input->post('kode_pos')),
                        'nama_pos'=>$this->db->escape_str($this->input->post('nama_pos'))
            );
            $where = array('id_pos' => $this->input->post('id'));
            $this->Model_app->update('pos', $data, $where);
            redirect($this->uri->segment(1).'/manajemenpos');
        }else{
            $id = $this->uri->segment(3);
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
                 $proses = $this->Model_app->edit('pos', array('id_pos' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('pos', array('id_pos' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses, 'title' => 'Edit Manajemen Pos');
            $this->template->load('administrator/template','administrator/mod_pos/view_pos_edit',$data);
        }
    }

    function delete_pos(){
        $this->cek_admin();
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $id = array('id_pos' => $this->uri->segment(3));
        }else{
            $id = array('id_pos' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('pos',$id);
        redirect($this->uri->segment(1).'/manajemenpos');
    }

    // Controller Modul Sub Pos
    function manajemensubpos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Manajemen Sub Pos';
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $data['record'] = $this->Model_app->view_join_one_tahun('subpos','pos','id_pos','id_subpos','ASC','tahun',''.$tahun.'');
        }else{
            $data['record'] = $this->Model_app->view_join_where_tahun('subpos','pos','id_pos',array('subpos.username'=>$this->session->username),'id_subpos','ASC',''.$tahun.'');
        }
        $this->template->load('administrator/template','administrator/mod_subpos/view_subpos',$data);
    }

    function tambah_subpos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Tambah Manajemen Sub Pos';
        if (isset($_POST['submit'])){
            $data = array(
                        'username'=>$this->session->username,
                        'id_pos'=>$this->db->escape_str($this->input->post('id_pos')),
                        'kd_rek2'=>$this->db->escape_str($this->input->post('kd_rek2')),
                        'kode_subpos'=>$this->db->escape_str($this->input->post('kode_subpos')),
                        'nama_subpos'=>$this->db->escape_str($this->input->post('nama_subpos')),
                        'tahun'=>''.$tahun.''
            );
            $this->Model_app->insert('subpos',$data);
            redirect($this->uri->segment(1).'/manajemensubpos');
        }else{
            $data['record'] = $this->Model_app->view_ordering_tahun('pos','id_pos','ASC','tahun',''.$tahun.'');
            $this->template->load('administrator/template','administrator/mod_subpos/view_subpos_tambah', $data);
        }
    }

    function edit_subpos(){
        $this->cek_admin();
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'id_pos'=>$this->db->escape_str($this->input->post('id_pos')),
                        'kd_rek2'=>$this->db->escape_str($this->input->post('kd_rek2')),
                        'kode_subpos'=>$this->db->escape_str($this->input->post('kode_subpos')),
                        'nama_subpos'=>$this->db->escape_str($this->input->post('nama_subpos'))
            );
            $where = array('id_subpos' => $this->input->post('id'));
            $this->Model_app->update('subpos', $data, $where);
            redirect($this->uri->segment(1).'/manajemensubpos');
        }else{
            $id = $this->uri->segment(3);
            $record = $this->Model_app->view_ordering('pos','id_pos','ASC');
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
                 $proses = $this->Model_app->edit('subpos', array('id_subpos' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('subpos', array('id_subpos' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses,'record' => $record, 'title'=>'Edit Manajemen Sub Pos');
            $this->template->load('administrator/template','administrator/mod_subpos/view_subpos_edit',$data);
        }
    }

    function delete_subpos(){
        $this->cek_admin();
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $id = array('id_subpos' => $this->uri->segment(3));
        }else{
            $id = array('id_subpos' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('subpos',$id);
        redirect($this->uri->segment(1).'/manajemensubpos');
    }

    // Controller Modul Pos Kegiatan
    function manajemensubposke(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Manajemen Pos Kegiatan';
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $data['record'] = $this->Model_app->view_join_tiga_tahun('tahun','subkegiatan','subpos','pos','id_tahun','id_subpos','id_pos','kode_pos','ASC','id_tahun',''.$tahun.'');
        }else{
            $data['record'] = $this->Model_app->view_join_where_tahun('subkegiatan','subpos','pos','id_subpos','id_pos',array('subkegiatan.username'=>$this->session->username),'poske','ASC','id_tahun',''.$tahun.'');
        }
        $this->template->load('administrator/template','administrator/mod_subposke/view_subposke',$data);
    }

    function tambah_subposke(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Tambah Manajemen Pos Kegiatan';
        if (isset($_POST['submit'])){
            $data = array(
                        'username'=>$this->session->username,
                        'id_subpos'=>$this->db->escape_str($this->input->post('id_subpos')),
                        'kd_rek3'=>$this->db->escape_str($this->input->post('kd_rek3')),
                        'poske'=>$this->db->escape_str($this->input->post('poske')),
                        'nama_kegiatan'=>$this->db->escape_str($this->input->post('nama_kegiatan')),
                        'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                        'nilai_anggaran'=>$this->db->escape_str($this->input->post('nilai_anggaran')),
                        'id_tahun'=>''.$tahun.''
            );
            $this->Model_app->insert('subkegiatan',$data);
            redirect($this->uri->segment(1).'/manajemensubposke');
        }else{
            $record = $this->Model_app->view_ordering('tahun','id_tahun','ASC');
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $proses = $this->Model_app->view_join_one_tahun('subpos','pos','id_pos','id_subpos','ASC','tahun',''.$tahun.'');
        }else{
            $proses = $this->Model_app->view_join_where('subpos','subpos','pos','id_subpos','id_pos',array('subkegiatan.username'=>$this->session->username),'poske','ASC');
        }
        $data = array('rows' => $proses,'record' => $record);
            $this->template->load('administrator/template','administrator/mod_subposke/view_subposke_tambah', $data);
        }
    }

    function edit_subposke(){
        $this->cek_admin();
        if (isset($_POST['submit'])){
            $data = array('username'=>$this->session->username,
                        'id_subpos'=>$this->db->escape_str($this->input->post('id_subpos')),
                        'kd_rek3'=>$this->db->escape_str($this->input->post('kd_rek3')),
                        'poske'=>$this->db->escape_str($this->input->post('poske')),
                        'nama_kegiatan'=>$this->db->escape_str($this->input->post('nama_kegiatan')),
                        'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                        'nilai_anggaran'=>$this->db->escape_str($this->input->post('nilai_anggaran')),
                        'id_tahun'=>$this->db->escape_str($this->input->post('id_tahun'))
            );
            $where = array('id_kegiatan' => $this->input->post('id'));
            $this->Model_app->update('subkegiatan', $data, $where);
            redirect($this->uri->segment(1).'/manajemensubposke');
        }else{
            $id = $this->uri->segment(3);
            $tahun = $this->Model_app->view_ordering('tahun','id_tahun','ASC');
            $record = $this->Model_app->view_join_two('subkegiatan','subpos','pos','id_subpos','id_pos','poske','ASC');
            if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
                 $proses = $this->Model_app->edit('subkegiatan', array('id_kegiatan' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('subkegiatan', array('id_kegiatan' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses,'record' => $record, 'tahun' => $tahun,'title'=>'Edit Manajemen Pos Kegiatan');
            $this->template->load('administrator/template','administrator/mod_subposke/view_subposke_edit',$data);
        }
    }

    function delete_subposke(){
        $this->cek_admin();
        if ($this->session->level=='admin' OR $this->session->level=='user2' OR $this->session->level=='wadek'){
            $id = array('id_kegiatan' => $this->uri->segment(3));
        }else{
            $id = array('id_kegiatan' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('subkegiatan',$id);
        redirect($this->uri->segment(1).'/manajemensubposke');
    }

    //Controller Manajemen Realisasi
    function realisasi(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Manajemen Realisasi';
        if ($this->session->level=='admin' OR $this->session->level=='user'){
            $data['record'] = $this->Model_app->view_join_empat('tahun','subkegiatan','realisasi','subpos','pos','id_tahun','id_kegiatan','id_subpos','id_pos','id_realisasi','ASC');
        }else{
            $data['record'] = $this->Model_app->view_join_tiga_where('realisasi','subkegiatan','subpos','pos','id_kegiatan','id_subpos','id_pos',array('subkegiatan.username'=>$this->session->username),'kode_pos','ASC');
        }
        $this->template->load('administrator/template','administrator/mod_realisasi/view_realisasi',$data);
    }

    function tambah_realisasi(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Tambah Manajemen Realisasi';
        
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/nota/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('nota');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('username'=>$this->session->username,
                        'id_pos'=>$this->db->escape_str($this->input->post('pos_id')),
                        'id_subpos'=>$this->db->escape_str($this->input->post('subpos_id')),
                        'id_kegiatan'=>$this->db->escape_str($this->input->post('subkegiatan_id')),
                        'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                        'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
                        'tahun'=>$this->session->tahun,
                        'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            }else{
                    $data = array('username'=>$this->session->username,
                        'id_pos'=>$this->db->escape_str($this->input->post('id_pos')),
                        'id_subpos'=>$this->db->escape_str($this->input->post('id_subpos')),
                        'id_kegiatan'=>$this->db->escape_str($this->input->post('id_kegiatan')),
                        'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                        'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
                        'tahun'=>$this->session->tahun,
                        'nota'=>$hasil['file_name'],
                        'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            }
            $this->Model_app->insert('realisasi',$data);
            redirect($this->uri->segment(1).'/realisasi');
        }else{
            if ($this->session->level=='admin' OR $this->session->level=='user'){
            $data['provinsi']=$this->Model_app->ambil_pos(); 
            $this->template->load('administrator/template1','administrator/mod_realisasi/view_realisasi_tambah', $data);
            }
        }
    }

    function edit_realisasi(){
        $this->cek_admin();
        $data['title'] = 'Edit Manajemen Realisasi';
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/nota/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('nota');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('nota') ==''){
                    $data = array(  'username'=>$this->session->username,
                                    'id_pos'=>$this->db->escape_str($this->input->post('pos_id')),
                                    'id_subpos'=>$this->db->escape_str($this->input->post('subpos_id')),
                                    'id_kegiatan'=>$this->db->escape_str($this->input->post('subkegiatan_id')),
                                    'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                                    'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
                                    'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('nota') ==''){
                    $data = array(  'username'=>$this->session->username,
                                    'id_pos'=>$this->db->escape_str($this->input->post('pos_id')),
                                    'id_subpos'=>$this->db->escape_str($this->input->post('subpos_id')),
                                    'id_kegiatan'=>$this->db->escape_str($this->input->post('subkegiatan_id')),
                                    'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                                    'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
                                    'nota'=>$hasil['file_name'],
                                    'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            }elseif ($hasil['file_name']=='' AND $this->input->post('nota') !=''){
                        $data = array(  'username'=>$this->db->escape_str($this->input->post('a')),
                                        'password'=>sha1($this->input->post('b')),
                                        'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                        'email'=>$this->db->escape_str($this->input->post('d')),
                                        'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                        'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('nota') !=''){
                    $data = array('username'=>$this->session->username,
                                    'id_pos'=>$this->db->escape_str($this->input->post('pos_id')),
                                    'id_subpos'=>$this->db->escape_str($this->input->post('subpos_id')),
                                    'id_kegiatan'=>$this->db->escape_str($this->input->post('subkegiatan_id')),
                                    'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
                                    'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
                                    'nota'=>$hasil['file_name'],
                                    'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            }

            // if ($hasil['file_name']==''){
            //         $data = array('username'=>$this->session->username,
            //             'id_pos'=>$this->db->escape_str($this->input->post('id_pos')),
            //             'id_subpos'=>$this->db->escape_str($this->input->post('id_subpos')),
            //             'id_kegiatan'=>$this->db->escape_str($this->input->post('id_kegiatan')),
            //             'nama_kegiatan'=>$this->db->escape_str($this->input->post('nama_kegiatan')),
            //             'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
            //             'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
            //             'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            // }else{
            //         $data = array('username'=>$this->session->username,
            //             'id_pos'=>$this->db->escape_str($this->input->post('id_pos')),
            //             'id_subpos'=>$this->db->escape_str($this->input->post('id_subpos')),
            //             'id_kegiatan'=>$this->db->escape_str($this->input->post('id_kegiatan')),
            //             'nama_kegiatan'=>$this->db->escape_str($this->input->post('nama_kegiatan')),
            //             'detail_kegiatan'=>$this->db->escape_str($this->input->post('detail_kegiatan')),
            //             'nilai_keluar'=>$this->db->escape_str($this->input->post('nilai_keluar')),
            //             'nota'=>$hasil['file_name'],
            //             'tgl'=>$this->db->escape_str($this->input->post('tgl')));
            // }
            $where = array('id_realisasi' => $this->input->post('id'));
            $this->Model_app->update('realisasi', $data, $where);
            redirect($this->uri->segment(1).'/realisasi');
        }else{
            $id = $this->uri->segment(3);
            $record = $this->Model_app->view_join_two('subkegiatan','subpos','pos','id_subpos','id_pos','kode_pos','ASC');
            if ($this->session->level=='admin' OR $this->session->level=='user'){
                 $proses = $this->Model_app->edit('realisasi', array('id_realisasi' => $id))->row_array();
            }else{
                $proses = $this->Model_app->edit('realisasi', array('id_realisasi' => $id, 'username' => $this->session->username))->row_array();
            }
            $provinsi=$this->Model_app->ambil_pos();
            $data = array('rows' => $proses,'record' => $record,'provinsi' => $provinsi);
            $this->template->load('administrator/template1','administrator/mod_realisasi/view_realisasi_edit',$data);
        }
    }

    function delete_realisasi(){
        $this->cek_admin();
        if ($this->session->level=='admin' OR $this->session->level=='user'){
            $id = array('id_realisasi' => $this->uri->segment(3));
        }else{
            $id = array('id_realisasi' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->Model_app->delete('realisasi',$id);
        redirect($this->uri->segment(1).'/realisasi');
    }

    //Saldo Pos Kegiatan
    function saldopos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Saldo Pos ';
        $this->template->load('administrator/template','administrator/mod_saldo/view_saldopos',$data);
    }

     //Saldo Pos Kegiatan
    function saldosubpos(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Saldo Sub Pos ';
        $this->template->load('administrator/template','administrator/mod_saldo/view_saldosubpos',$data);
    }

     //Saldo Pos Kegiatan
    function saldoposkegiatan(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Saldo Pos Kegiatan ';
        $this->template->load('administrator/template','administrator/mod_saldo/view_saldoposkegiatan',$data);
    }

    function saldoall(){
        $this->cek_admin();
        $tahun=$this->session->tahun;
        $data['title'] = 'Saldo Pos Kegiatan';
        $this->template->load('administrator/template','administrator/mod_saldo/view_saldoall',$data);
    }

    // Controller Modul Laporan

    function laporan(){
        $this->cek_admin();
        $data['title'] = 'Laporan Keuangan';
        $this->template->load('administrator/template','administrator/mod_laporan/view_laporan', $data);
    }

    function laporanall(){
        $this->cek_admin();
        $data['title'] = 'Laporan Keuangan Keseluruhan';
        $this->template->load('administrator/template','administrator/mod_laporan/view_laporanall', $data);
    }

    function tampil_data(){
        $vtanggal=$this->input->post('vtanggal');
        $data['tampil_data']=$this->Model_app->tampil_data($vtanggal);
        $data['tampil_data1']=$this->Model_app->tampil_data1($vtanggal);
        $this->load->view('administrator/mod_laporan/tampil_data',$data);
    }

    function cetak_laporan(){
        $data['title'] = 'Cetak Laporan Keuangan';
        $vtanggal=$this->input->post('vtanggal');
        $data['tampil_data']=$this->Model_app->tampil_data($vtanggal);
        $data['tampil_data1']=$this->Model_app->tampil_data1($vtangal);
        $this->load->view('administrator/mod_laporan/cetak_laporan',$data);
    }

    function cek_admin(){
        if(!$this->session->userdata('level')) {
            redirect ('administrator/index');
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('administrator');
    }
}